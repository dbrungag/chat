package ru.nsu.drubtsov.Server;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map.Entry;

class Handler implements Runnable {
    private Entry<BufferedWriter, Pair> currUser;
    private HashMap<BufferedWriter, Pair> users;
    private BufferedReader usInStr;
    private BufferedWriter usOutStr;
    private BufferedWriter confWriter;
    private String username;
    private String userDisapp = "- disconnected\n";
    private String classPref = "ru.nsu.drubtsov.Server.";
    private String sourcePref = "Вы: ";
    private String otherPref;

    public Handler(Entry<BufferedWriter, Pair> user, HashMap<BufferedWriter, Pair> usList, BufferedWriter confWr) {
        currUser = user;
        users = usList;
        confWriter = confWr;
        username = currUser.getValue().getValue();
        otherPref = username + ": ";
        usInStr = currUser.getValue().getKey();
        usOutStr = currUser.getKey();
    }

    public void run() {
        try {
            Iterator<Entry<BufferedWriter, Pair>> iter;
            String message = usInStr.readLine(), msgToSource = sourcePref + message + "\n";
            if (message.charAt(0) == '/') {
                String req = message.substring(1).toUpperCase();
                ServerReaction reaction;
                req = classPref + req;
                try {
                    Class cls = Class.forName(req);
                    Constructor[] constrs = cls.getConstructors();
                    Class[] params = constrs[0].getParameterTypes();
                    Constructor constr = cls.getConstructor(params);
                    reaction = (ServerReaction) constr.newInstance();
                    reaction.react(usOutStr, users, confWriter);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException exc) {
                    System.out.println(exc.getMessage());
                }
            } else {
                message = otherPref + message + "\n";
                iter = users.entrySet().iterator();
                synchronized (users) {
                    while (iter.hasNext()) {
                        BufferedWriter tmpStr = iter.next().getKey();
                        if (tmpStr.equals(usOutStr)) {
                            usOutStr.write(msgToSource);
                            usOutStr.flush();
                            continue;
                        }
                        tmpStr.write(message);
                        tmpStr.flush();
                    }
                }
            }
        }catch (IOException exc) {
            System.err.println(exc.getMessage());
            try {
                synchronized (confWriter) {
                    confWriter.write(users.get(usOutStr).getValue() + userDisapp);
                    confWriter.flush();
                }
                synchronized (users){
                    users.remove(usOutStr);
                }
                usInStr.close();
                usOutStr.close();
            } catch (IOException clExc) {
                System.err.println(clExc.getMessage());
            }
        }
    }
}