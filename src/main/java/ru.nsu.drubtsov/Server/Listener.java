package ru.nsu.drubtsov.Server;

import java.io.*;
import java.util.concurrent.ExecutorService;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class Listener implements Runnable {
    private ExecutorService pool;
    private HashMap<BufferedWriter, Pair> users;
    private BufferedWriter confWriter;

    public Listener(HashMap<BufferedWriter, Pair> usList, ExecutorService tPool, BufferedWriter confWr){
        users = usList;
        pool = tPool;
        confWriter = confWr;
    }

    public void run () {
        try {
            Iterator<Entry<BufferedWriter, Pair>> iter;
            while(true) {
                synchronized (users) {
                    iter = users.entrySet().iterator();
                    while (iter.hasNext()) {
                        Entry<BufferedWriter, Pair> user = iter.next();
                        BufferedReader tmpStr = user.getValue().getKey();
                        if (tmpStr.ready()) {
                            pool.execute(new Handler(user, users, confWriter));
                        }
                    }
                }
                Thread.sleep(1000);
            }
        } catch (IOException | InterruptedException exc) {
            System.out.println(exc.getMessage());
        }
    }
}
