package ru.nsu.drubtsov.Server;

import java.net.Socket;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

class Authorization implements Runnable {
    private Socket socket;
    private HashMap<BufferedWriter, Pair> users;
    private BufferedReader usInStr;
    private BufferedWriter usOutStr;
    private BufferedWriter confWriter;
    private String userApp = "- connected\n";
    private String userDisapp = "- disconnected\n";
    private String greetPart = "Добро пожаловать, ";
    private String joinPart = " присоединился\n";

    public Authorization(Socket tSocket, HashMap<BufferedWriter, Pair> usList, BufferedWriter confWr) throws java.io.IOException{
        users = usList;
        socket = tSocket;
        confWriter = confWr;
        usInStr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        usOutStr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    public void run() {
        try {
            String username = usInStr.readLine();
            Iterator<Entry<BufferedWriter, Pair>> iter;
            synchronized (users){
                users.put(usOutStr, new Pair(usInStr, username));
                synchronized (confWriter){
                    confWriter.write(username + userApp);
                    confWriter.flush();
                }
                usOutStr.write( greetPart + username + "\n");
                usOutStr.flush();
                iter = users.entrySet().iterator();
                while (iter.hasNext()) {
                    Entry<BufferedWriter, Pair> elem = iter.next();
                    BufferedWriter tmpStr = elem.getKey();
                    if (tmpStr.equals(usOutStr))
                        continue;
                    tmpStr.write(username + joinPart);
                    tmpStr.flush();
                    }
            }
        } catch (IOException exc) {
            System.err.println(exc.getMessage());
            try {
                synchronized (confWriter) {
                    confWriter.write(users.get(usOutStr).getValue() + userDisapp);
                    confWriter.flush();
                }
                synchronized (users){
                    users.remove(usOutStr);
                }
                usInStr.close();
                usOutStr.close();
            } catch (IOException clExc) {
                System.err.println(clExc.getMessage());
            }
        }
    }
}