package ru.nsu.drubtsov.Server;

import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Server {
    private ServerSocket serverSocket;
    private ExecutorService pool;
    private BufferedWriter confWriter;
    private HashMap<BufferedWriter, Pair> users;
    public Server(int port, int poolSize, String pathname) throws IOException {
        serverSocket = new ServerSocket(port);
        pool = Executors.newFixedThreadPool(poolSize);
        users = new HashMap<>();
        confWriter = new BufferedWriter(new FileWriter(new File(pathname), true));
}

    public void run() {
        try{
            try {
                Thread thread = new Thread(new Listener(users, pool, confWriter));
                thread.start();
                while (true)
                {
                    Socket clSocket = serverSocket.accept();
                    pool.execute(new Authorization(clSocket, users, confWriter));
                }
            } finally {
                serverSocket.close();
                confWriter.close();
            }
        } catch (IOException ex) {
            pool.shutdown();
        }
    }
}