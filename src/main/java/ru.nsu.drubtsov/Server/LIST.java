package ru.nsu.drubtsov.Server;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class LIST implements ServerReaction {
    private String askEnd = "- asked for list\n";
    public void react(BufferedWriter usOutStr, HashMap<BufferedWriter, Pair> usList, BufferedWriter confWr) throws java.io.IOException {
        String ans = "Всего пользователей " + usList.size() + ": ";
        synchronized (usList) {
            Iterator<Entry<BufferedWriter, Pair>> iter = usList.entrySet().iterator();
            while (true) {
                String username = iter.next().getValue().getValue();
                if (!iter.hasNext()) {
                    ans += username;
                    ans += "\n";
                    break;
                }
                ans += username;
                ans += ", ";
            }
        }
        synchronized (usOutStr) {
            usOutStr.write(ans);
            usOutStr.flush();
        }
        synchronized (confWr) {
            confWr.write(usList.get(usOutStr).getValue() + askEnd);
            confWr.flush();
        }
    }
}
