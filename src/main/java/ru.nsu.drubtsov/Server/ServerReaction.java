package ru.nsu.drubtsov.Server;

import java.io.BufferedWriter;
import java.util.HashMap;

public interface ServerReaction{
    public void react(BufferedWriter usOutStr, HashMap<BufferedWriter, Pair> usList, BufferedWriter confWr) throws java.io.IOException;
}
