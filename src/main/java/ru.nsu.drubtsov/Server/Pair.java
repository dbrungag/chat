package ru.nsu.drubtsov.Server;

import java.io.BufferedReader;
import java.util.Map;

public class Pair implements Map.Entry {
    private BufferedReader inStr;
    private String username;

    public Pair(BufferedReader stream, String word){
        inStr = stream;
        username = word;
    }
    public BufferedReader getKey() {
        return inStr;
    }
    public String getValue() {
        return username;
    }
    public Integer setValue(Object o) {
        username = o.toString();
        return this.hashCode();
    }
}
