package ru.nsu.drubtsov.Server;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        if (args.length != 3){
            System.out.println("Usage: programm port size_of_thread_pool pathname_for_conf");
            return;
        }
        try{
            Server server = new Server(Integer.parseInt(args[0]), Integer.parseInt(args[1]), args[2]);
            server.run();
        } catch (IOException exc) {
            System.err.println(exc.getMessage());
        }
    }
}
