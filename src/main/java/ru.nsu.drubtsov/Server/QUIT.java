package ru.nsu.drubtsov.Server;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class QUIT implements ServerReaction {
    private String userDisapp = "- disconnected\n";
    public void react(BufferedWriter usOutStr, HashMap<BufferedWriter, Pair> usList, BufferedWriter confWr) throws java.io.IOException{
        String exMsg = usList.get(usOutStr).getValue() + " вышел из чата\n";
        synchronized (usList) {
            Iterator<Entry<BufferedWriter, Pair>> iter = usList.entrySet().iterator();
            while (iter.hasNext()) {
                BufferedWriter tmpStr = iter.next().getKey();
                if (tmpStr.equals(usOutStr)) {
                    continue;
                }
                tmpStr.write(exMsg);
                tmpStr.flush();
            }
            synchronized (confWr) {
                confWr.write(usList.get(usOutStr).getValue() + userDisapp);
                confWr.flush();
            }
            usList.remove(usOutStr);
        }
    }
}
