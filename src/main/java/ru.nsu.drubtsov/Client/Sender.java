package ru.nsu.drubtsov.Client;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.PipedInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.net.Socket;

public class Sender implements Runnable {
    private PipedInputStream inPipe;
    private GUI gui;
    private Socket clientSocket;
    private String greetingMsg = "Введите ваше имя";
    private String endOfSession = "/quit";

    public Sender(Socket clientS, GUI pgui, PipedInputStream pipe) {
        inPipe = pipe;
        clientSocket = clientS;
        gui = pgui;
    }

    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inPipe));
            BufferedWriter outStr = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            try {
                gui.print(greetingMsg);
                outStr.write(reader.readLine() + "\n");
                outStr.flush();
                while (true) {
                    String message = reader.readLine();
                    outStr.write(message + "\n");
                    outStr.flush();
                    if (message.equals(endOfSession))
                        break;
                }
            } finally {
                reader.close();
                outStr.close();
            }
        } catch (IOException exc) {
            System.out.println(exc.toString());
        }
    }
}
