package ru.nsu.drubtsov.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Receiver implements Runnable {
    private Socket clientSocket;
    private GUI gui;

    public Receiver(Socket clientS, GUI pgui){
        clientSocket = clientS;
        gui = pgui;
    }

    public void run () {
        try {
            BufferedReader inStr = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            try {
                while (true) {
                    String serverMsg = inStr.readLine();
                    gui.print(serverMsg);
                }
            } finally {
                inStr.close();
            }
        } catch (IOException exc) {
            System.out.println(exc.toString());
        }
    }
}
