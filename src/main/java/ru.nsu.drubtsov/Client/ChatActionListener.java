package ru.nsu.drubtsov.Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChatActionListener implements ActionListener {
    private GUI gui;
    public ChatActionListener(GUI pgui){
        gui = pgui;
    }

    public void actionPerformed(ActionEvent e) {
        gui.react();
    }
}
