package ru.nsu.drubtsov.Client;

import javax.swing.*;
import java.awt.BorderLayout;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.PipedInputStream;
import java.io.OutputStreamWriter;
import java.io.PipedOutputStream;

public class GUI {
    private JFrame frame;
    private JPanel panel;
    private JLabel label;
    private JTextField msgToSend;
    private JTextArea msgList;
    private JButton send;
    private JScrollPane scrollPane;
    private ChatActionListener listener;
    private BufferedWriter outStr;
    private int numOfCols = 16;
    private int frameWidth = 640;
    private int frameHeight = 480;
    private String appTitle = "Чат";
    private String labelTitle = "Введите текст";
    private String buttonTitle = "Отправить";
    private String empty = "";

    public GUI(PipedInputStream inStr) throws java.io.IOException{
        listener = new ChatActionListener(this);
        outStr = new BufferedWriter(new OutputStreamWriter(new PipedOutputStream(inStr)));
        frame = new JFrame(appTitle);
        panel = new JPanel();
        label = new JLabel(labelTitle);
        msgToSend = new JTextField(numOfCols);
        send = new JButton(buttonTitle);
        msgList = new JTextArea();
        scrollPane = new JScrollPane(msgList);
        panel.add(label);
        panel.add(msgToSend);
        panel.add(send);
        msgList.setEditable(false);
        frame.setSize(frameWidth, frameHeight);
        frame.getContentPane().add(BorderLayout.CENTER, scrollPane);
        frame.getContentPane().add(BorderLayout.SOUTH, panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        send.addActionListener(listener);
    }

    public void print(String msg) {
        msgList.append(msg + "\n");
    }

    public void react() {
        try {
            String msg = msgToSend.getText();
            if (!msg.isEmpty()) {
                outStr.write(msgToSend.getText() + "\n");
                outStr.flush();
                msgToSend.setText(empty);
            }
        } catch (IOException exc) {
            System.out.println(exc.toString());
        }
    }

    public void close() throws java.io.IOException{
        outStr.close();
        frame.dispose();
    }
}
