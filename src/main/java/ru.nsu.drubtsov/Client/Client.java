package ru.nsu.drubtsov.Client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.lang.InterruptedException;

public class Client {
    public static void main(String[] args) {
        if (args.length != 2){
            System.out.println("Usage: programm ip port");
            return;
        }
        try {
            Socket clientSocket = new Socket(InetAddress.getByName(args[0]), Integer.parseInt(args[1]));
            PipedInputStream inPipe = new PipedInputStream();
            GUI gui = new GUI(inPipe);
            Sender sender = new Sender(clientSocket, gui, inPipe);
            Receiver receiver = new Receiver(clientSocket, gui);
            try {
                Thread thread1 = new Thread(sender);
                Thread thread2 = new Thread(receiver);
                thread1.start();
                thread2.start();
                thread1.join();
                thread2.join();
            } finally {
                gui.close();
                inPipe.close();
                clientSocket.close();
            }
        } catch (IOException | InterruptedException exc) {
            System.out.println(exc.getMessage());
        }
    }

}
